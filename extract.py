import argparse
import xml.etree.ElementTree as ET

# IMPORTANT!
# Convert input pdf with `inkscape -l <output_svg> <input_pdf>` to preserve dom structure

template = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
   version="1.1" xml:space="preserve" width="62mm" height="29mm" viewBox="0 0 234.3307 109.6063">
   <g transform="matrix(1.3333333,0,0,1.3333333,48.188619,15.330485)"/>
</svg>"""

def extract_groups(filename):
    filename_split = filename.split('.')
    filename_template = '_{}.'.join((''.join(filename_split[:-1]), filename_split[-1]))
    ns = dict(svg='http://www.w3.org/2000/svg')
    ET.register_namespace('svg', ns['svg'])
    tree = ET.parse(filename)
    root = tree.getroot()
    candidates = root.findall('./svg:g/svg:g/svg:g/svg:g/', ns)
    for i, group in enumerate(filter(lambda x: len(x) == 3, candidates)):
        nroot = ET.fromstring(template)
        nroot[0].append(group)
        ET.ElementTree(nroot).write(filename_template.format(i))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract stamps from Deutsche Post Internetmarke pages')
    parser.add_argument('svg', help='The SVG file to process')
    args = parser.parse_args()
    extract_groups(args.svg)

